package repositories

import (
	"image"

	"bitbucket.org/caiotava/spotippos/entities"
)

type PropertieCollection struct {
	FoundProperties int                  `json:"foundProperties"`
	Properties      []entities.Propertie `json:"properties"`
}

type PropertieRepository interface {
	SearchByBoundaries(image.Rectangle) (PropertieCollection, error)
	GetById(int) (entities.Propertie, error)
	Save(*entities.Propertie) error
	Delete(int) error
}

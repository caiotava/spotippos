package repositories

import (
	//"fmt"
	"image"
	"testing"

	"bitbucket.org/caiotava/spotippos/entities"
)

func TestPropertiRepositoryMemorySearchByBoundaries(t *testing.T) {
	repo := createPopulatedPropertieRepositoryMemory()
	rect := image.Rect(500, 500, 1258, 999)

	result, _ := repo.SearchByBoundaries(rect)

	if result.FoundProperties != 2 {
		t.Errorf("Expected 2 properties returned, but got %v", result.FoundProperties)
	}
}

func TestPropertiRepositoryMemoryGetById(t *testing.T) {
	repo := createPopulatedPropertieRepositoryMemory()

	p, _ := repo.GetById(2)

	if p.Title != "bar" {
		t.Errorf("Expected new 2-Propertie title equals \"bar\" and got \"%v\"", p.Title)
	}
}

func TestPropertiRepositoryMemoryGetByIdNotFound(t *testing.T) {
	repo := createPopulatedPropertieRepositoryMemory()

	p, err := repo.GetById(123456)

	if err == nil {
		t.Errorf("Expected NotFound propertie and got \"%v\"", p.Title)
	}
}

func TestPropertiRepositoryMemorySaveUpdated(t *testing.T) {
	repo := createPopulatedPropertieRepositoryMemory()

	p, _ := repo.GetById(1)
	p.Title = "Foo Bar New"

	repo.Save(&p)

	p, _ = repo.GetById(1)

	if p.Title != "Foo Bar New" {
		t.Errorf("Expected propertie title equals \"Foo Bar New\" and got \"%v\"", p.Title)
	}
}

func TestPropertiRepositoryMemoryDelete(t *testing.T) {
	repo := createPopulatedPropertieRepositoryMemory()

	_ = repo.Delete(1)
	p, err := repo.GetById(1)

	if err == nil {
		t.Errorf("Expected NotFound propertie and got \"%v\"", p.Title)
	}
}

func TestPropertiRepositoryMemoryNewPropertieRepoMemory(t *testing.T) {
	repo := NewPropertieRepoMemory()

	if len(repo.properties) != 0 {
		t.Errorf("Expected 0 properties and got \"%v\"", len(repo.properties))
	}
}

func TestPropertiRepositoryMemorySavePropertieIdGreaterLastId(t *testing.T) {
	repo := createPopulatedPropertieRepositoryMemory()
	propertie := entities.Propertie{Id: 1000}

	_ = repo.Save(&propertie)

	if repo.lastId != 1000 {
		t.Errorf("Expected LastId equals 1000 and got \"%v\"", repo.lastId)
	}
}

func createPopulatedPropertieRepositoryMemory() PropertieRepositoryMemory {
	properties := []entities.Propertie{
		{Title: "foo", Price: 1234000, Description: "foo desc", X: 1257, Y: 928, Beds: 3, Baths: 2, SquareMeters: 61},
		{Title: "bar", Price: 1250000, Description: "bar desc", X: 222, Y: 444, Beds: 4, Baths: 3, SquareMeters: 210},
		{Title: "baz", Price: 5400000, Description: "baz desc", X: 667, Y: 556, Beds: 1, Baths: 1, SquareMeters: 42},
	}

	repo := PropertieRepositoryMemory{properties: make(map[int]entities.Propertie)}

	for _, p := range properties {
		repo.Save(&p)
	}

	return repo
}

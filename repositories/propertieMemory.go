package repositories

import (
	"errors"
	"image"

	"bitbucket.org/caiotava/spotippos/entities"
)

type PropertieRepositoryMemory struct {
	properties map[int]entities.Propertie
	lastId     int
}

func (r *PropertieRepositoryMemory) SearchByBoundaries(rect image.Rectangle) (PropertieCollection, error) {
	found := []entities.Propertie{}

	for i := range r.properties {
		point := image.Pt(r.properties[i].X, r.properties[i].Y)

		if pointInRect(point, rect) {
			found = append(found, r.properties[i])
		}
	}

	result := PropertieCollection{
		FoundProperties: len(found),
		Properties:      found,
	}

	return result, nil
}

func (r *PropertieRepositoryMemory) GetById(id int) (entities.Propertie, error) {
	p, ok := r.properties[id]

	if ok {
		return p, nil
	}

	return p, errors.New("Propertie not found")
}

func (r *PropertieRepositoryMemory) Save(p *entities.Propertie) error {
	if p.Id <= 0 {
		r.lastId++
		p.Id = r.lastId
	}

	if p.Id > r.lastId {
		r.lastId = p.Id
	}

	p.Provinces = getProvincesNames(*p)
	r.properties[p.Id] = *p

	return nil
}

func (r *PropertieRepositoryMemory) Delete(id int) error {
	delete(r.properties, id)

	return nil
}

func NewPropertieRepoMemory() PropertieRepositoryMemory {
	return PropertieRepositoryMemory{
		properties: make(map[int]entities.Propertie),
	}
}

func getProvincesNames(p entities.Propertie) []string {
	names := []string{}

	for _, province := range entities.Provinces {
		point := image.Pt(p.X, p.Y)

		if pointInRect(point, province.Boundaries) {
			names = append(names, province.Name)
		}
	}

	return names
}

func pointInRect(p image.Point, r image.Rectangle) bool {
	return r.Min.X <= p.X && p.X <= r.Max.X &&
		r.Min.Y <= p.Y && p.Y <= r.Max.Y
}

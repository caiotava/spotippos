package validations

import (
	"net/http"

	"bitbucket.org/caiotava/spotippos/entities"
	"github.com/thedevsaddam/govalidator"
)

func NewPropertieQueryValidator(r *http.Request) *govalidator.Validator {
	rules := govalidator.MapData{
		"ax": []string{"required", "numeric"},
		"ay": []string{"required", "numeric"},
		"bx": []string{"required", "numeric"},
		"by": []string{"required", "numeric"},
	}

	opts := govalidator.Options{
		Request: r,
		Rules:   rules,
	}

	return govalidator.New(opts)
}

func NewPropertieRequestValidator(p *entities.Propertie, r *http.Request) *govalidator.Validator {
	rules := govalidator.MapData{
		"title":        []string{"required"},
		"price":        []string{"required"},
		"description":  []string{"required"},
		"x":            []string{"required", "numeric_between:0,1400"},
		"y":            []string{"required", "numeric_between:0,1000"},
		"beds":         []string{"required", "numeric_between:1,5"},
		"baths":        []string{"required", "numeric_between:1,4"},
		"squareMeters": []string{"required", "numeric_between:20,240"},
	}

	opts := govalidator.Options{
		Request:         r,
		Rules:           rules,
		Data:            p,
		RequiredDefault: true,
	}

	return govalidator.New(opts)
}

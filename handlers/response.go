package handlers

import (
	"encoding/json"
	"net/http"
	"net/url"
)

func responseJson(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-type", "application/json")

	encoder := json.NewEncoder(w)
	err := encoder.Encode(v)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func responseJsonErrBag(w http.ResponseWriter, errBag url.Values) {
	err := map[string]interface{}{"validationError": errBag}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	responseJson(w, err)
}

func responseError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusNotFound)
}

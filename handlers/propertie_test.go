package handlers

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
	//"fmt"

	"bitbucket.org/caiotava/spotippos/entities"
	"bitbucket.org/caiotava/spotippos/repositories"
	"github.com/julienschmidt/httprouter"
)

func TestPropertieHandlerIndex(t *testing.T) {
	request, response, handler, params := setupPropertieTest("GET", "")

	newUrl, _ := url.Parse("/properties?ax=1&ay=1&bx=10&by=100")
	request.URL = newUrl

	handler.Index(response, request, params)

	if response.Code != http.StatusOK {
		t.Errorf("Expected 200 OK got %v", response.Code)
	}
}

func TestPropertieHandlerIndexInvalidParameters(t *testing.T) {
	request, response, handler, params := setupPropertieTest("GET", "")

	handler.Index(response, request, params)

	if response.Code != http.StatusBadRequest {
		t.Errorf("Expected BadRequest got %v", response.Code)
	}
}

func TestPropertieHandlerShow(t *testing.T) {
	request, response, handler, params := setupPropertieTest("GET", "")

	propertie := insertPropertieOnRepositoryTest(handler.Repository)
	params = append(params, httprouter.Param{"id", strconv.Itoa(propertie.Id)})

	handler.Show(response, request, params)

	if response.Code != http.StatusOK {
		t.Errorf("Expected 200 OK got %v", response.Code)
	}
}

func TestPropertieHandlerShowNotFound(t *testing.T) {
	request, response, handler, params := setupPropertieTest("GET", "")

	handler.Show(response, request, params)

	if response.Code != http.StatusNotFound {
		t.Errorf("Expected 404 OK got %v", response.Code)
	}
}

func TestPropertieHandlerCreate(t *testing.T) {
	propertieJson := `{"title": "I","price": 6,"description": "La","x" :1,"y": 9,"beds": 3,"baths": 2,"squareMeters": 61}`
	request, response, handler, params := setupPropertieTest("POST", propertieJson)

	handler.Create(response, request, params)

	if response.Code != http.StatusCreated {
		t.Errorf("Expected 201 Created got %v", response.Code)
	}
}

func TestPropertieHandlerCreateInvalidInput(t *testing.T) {
	request, response, handler, params := setupPropertieTest("POST", `{"id": "Foo", "title": 1234}`)

	handler.Create(response, request, params)

	if response.Code != http.StatusBadRequest {
		t.Errorf("Expected BadRequest got %v", response.Code)
	}
}

func TestPropertieHandlerUpdate(t *testing.T) {
	propertieJson := `{"title": "I","price": 6,"description": "La","x" :1,"y": 9,"beds": 3,"baths": 2,"squareMeters": 61}`
	request, response, handler, params := setupPropertieTest("POST", propertieJson)

	params = append(params, httprouter.Param{"id", "1"})

	handler.Update(response, request, params)

	if response.Code != http.StatusNoContent {
		t.Errorf("Expected 204 NoContent got %v", response.Code)
	}
}

func TestPropertieHandlerDelete(t *testing.T) {
	request, response, handler, params := setupPropertieTest("DELETE", "")

	propertie := insertPropertieOnRepositoryTest(handler.Repository)
	params = append(params, httprouter.Param{"id", strconv.Itoa(propertie.Id)})

	handler.Delete(response, request, params)

	if response.Code != http.StatusNoContent {
		t.Errorf("Expected 204 NotFound got %v", response.Code)
	}
}

func TestPropertieHandlerDeleteNotFound(t *testing.T) {
	request, response, handler, params := setupPropertieTest("DELETE", "")

	handler.Delete(response, request, params)

	if response.Code != http.StatusNotFound {
		t.Errorf("Expected 404 NotFound got %v", response.Code)
	}
}

func insertPropertieOnRepositoryTest(repo repositories.PropertieRepository) entities.Propertie {
	propertie := entities.Propertie{
		Title:        "I",
		Price:        6,
		Description:  "La",
		X:            1,
		Y:            9,
		Beds:         3,
		Baths:        2,
		SquareMeters: 61,
	}

	repo.Save(&propertie)

	return propertie
}

func setupPropertieTest(method string, contentBody string) (*http.Request, *httptest.ResponseRecorder, PropertieHandler, httprouter.Params) {
	repo := repositories.NewPropertieRepoMemory()
	handler := PropertieHandler{Repository: &repo}
	request := httptest.NewRequest(method, "/properties", strings.NewReader(contentBody))
	response := httptest.NewRecorder()
	params := httprouter.Params{}

	return request, response, handler, params
}

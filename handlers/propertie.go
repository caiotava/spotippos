package handlers

import (
	"fmt"
	"image"
	"net/http"
	"net/url"
	"strconv"

	"bitbucket.org/caiotava/spotippos/entities"
	"bitbucket.org/caiotava/spotippos/repositories"
	"bitbucket.org/caiotava/spotippos/validations"
	"github.com/julienschmidt/httprouter"
)

type PropertieHandler struct {
	Repository repositories.PropertieRepository
}

func (h *PropertieHandler) Index(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	v := validations.NewPropertieQueryValidator(r)
	errBag := v.Validate()

	if len(errBag) != 0 {
		responseJsonErrBag(w, errBag)
		return
	}

	rect := rectangleBoundariesFromQuery(r)
	propertieCollection, _ := h.Repository.SearchByBoundaries(rect)

	responseJson(w, propertieCollection)
}

func (h *PropertieHandler) Show(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := getIdFromParams(ps)
	p, err := h.Repository.GetById(id)

	if err != nil {
		responseError(w, err)
		return
	}

	responseJson(w, p)
}

func (h *PropertieHandler) Create(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	p, ok := h.savePropertie(w, r, ps)

	if ok {
		w.Header().Set("Location", fmt.Sprintf("%s/%v", r.URL.Path, p.Id))
		w.WriteHeader(http.StatusCreated)
	}
}

func (h *PropertieHandler) Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	_, ok := h.savePropertie(w, r, ps)

	if ok {
		w.WriteHeader(http.StatusNoContent)
	}
}

func (h *PropertieHandler) Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := getIdFromParams(ps)
	p, err := h.Repository.GetById(id)

	if err != nil {
		responseError(w, err)
		return
	}

	err = h.Repository.Delete(p.Id)

	if err != nil {
		responseError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (h *PropertieHandler) savePropertie(w http.ResponseWriter, r *http.Request, ps httprouter.Params) (entities.Propertie, bool) {
	p, errBag := createPropertieFromRequest(r)
	p.Id = getIdFromParams(ps)

	if len(errBag) != 0 {
		responseJsonErrBag(w, errBag)
		return p, false
	}

	err := h.Repository.Save(&p)

	if err != nil {
		responseError(w, err)
		return p, false
	}

	return p, true
}

func getIdFromParams(ps httprouter.Params) int {
	id, _ := strconv.Atoi(ps.ByName("id"))

	return id
}

func rectangleBoundariesFromQuery(r *http.Request) image.Rectangle {
	ax, _ := strconv.Atoi(r.URL.Query().Get("ax"))
	ay, _ := strconv.Atoi(r.URL.Query().Get("ay"))
	bx, _ := strconv.Atoi(r.URL.Query().Get("bx"))
	by, _ := strconv.Atoi(r.URL.Query().Get("by"))

	return image.Rect(ax, ay, bx, by)
}

func createPropertieFromRequest(r *http.Request) (entities.Propertie, url.Values) {
	p := entities.Propertie{}
	v := validations.NewPropertieRequestValidator(&p, r)

	return p, v.ValidateJSON()
}

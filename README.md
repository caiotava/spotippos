[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/caiotava/spotippos)](https://goreportcard.com/report/bitbucket.org/caiotava/spotippos)
[![Coverage Status](https://coveralls.io/repos/bitbucket/caiotava/spotippos/badge.svg?branch=master)](https://coveralls.io/bitbucket/caiotava/spotippos?branch=master)
![Build Status](https://img.shields.io/bitbucket/pipelines/caiotava/spotippos.svg)

# Sobre Spotippos API

É uma API Rest para ajudar os Bytes a encontrarem a casa dos seus sonhos, em uma das adoráveis províncias de Spotippos.

## Ambiente de teste

[https://spotippos-caiotava.herokuapp.com](https://spotippos-caiotava.herokuapp.com)

## Criando um ambiente local

O servidor por padrão usa a porta **8080**, mas isso pode ser configurado com a variável de ambiente **$PORT**

**Compilando manualmente**


``` bash
	$ go get bitbucket.org/caiotava/spotippos/
	$ cd `(go env GOPATH)`/src/bitbucket.org/caiotava/spotippos
	$ go build -o spotippos main.go
	$ ./spotippos
```

**Usando Docker Compose**


``` bash
    $ git clone https://bitbucket.org/caiotava/spotippos.git
	$ cd spotippos
	$ docker-compose up -d spotippos-api 
```

**Testando ambiente local**

1. Acesse o link http://localhost:8080/properties/1

## Documentação simples da API

https://documenter.getpostman.com/view/1026251/RWTeX3Be
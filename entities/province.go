package entities

import (
	"image"
)

var Provinces = []Province{
	{Name: "Gode", Boundaries: image.Rect(0, 1000, 600, 500)},
	{Name: "Ruja", Boundaries: image.Rect(400, 1000, 1100, 500)},
	{Name: "Jaby", Boundaries: image.Rect(1100, 1000, 1400, 500)},
	{Name: "Scavy", Boundaries: image.Rect(0, 500, 600, 0)},
	{Name: "Groola", Boundaries: image.Rect(600, 500, 800, 0)},
	{Name: "Nova", Boundaries: image.Rect(800, 500, 1400, 0)},
}

type Province struct {
	Name       string
	Boundaries image.Rectangle
}

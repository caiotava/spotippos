package entities

type Propertie struct {
	Id           int      `json:"id"`
	Title        string   `json:"title"`
	Price        float32  `json:"price"`
	Description  string   `json:"description"`
	X            int      `json:"x"`
	Y            int      `json:"y"`
	Beds         int      `json:"beds"`
	Baths        int      `json:"baths"`
	Provinces    []string `json:"provinces"`
	SquareMeters int      `json:"squareMeters"`
}

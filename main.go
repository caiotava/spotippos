package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/caiotava/spotippos/handlers"
	"bitbucket.org/caiotava/spotippos/importers"
	"bitbucket.org/caiotava/spotippos/repositories"
	"github.com/julienschmidt/httprouter"
)

var urlPropertiesJson string = "https://bitbucket.org/caiotava/spotippos/raw/0bb2905ec2b4b98a0403f469479c0bf1faae541f/assets/properties.json"

func main() {
	router := httprouter.New()
	port := getServerPort()
	propertieRepository := repositories.NewPropertieRepoMemory()
	propertieHandler := handlers.PropertieHandler{&propertieRepository}

	importers.ImportPropertiesFromUrl(&propertieRepository, urlPropertiesJson)

	router.GET("/properties", propertieHandler.Index)
	router.GET("/properties/:id", propertieHandler.Show)
	router.POST("/properties", propertieHandler.Create)
	router.PUT("/properties/:id", propertieHandler.Update)
	router.DELETE("/properties/:id", propertieHandler.Delete)

	log.Printf("Server starting on port %v\n", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func getServerPort() string {
	if envPort := os.Getenv("PORT"); envPort != "" {
		return envPort
	}

	return "8080"
}

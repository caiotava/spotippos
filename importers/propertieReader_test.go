package importers

import (
	"errors"
	"strings"
	"testing"

	"bitbucket.org/caiotava/spotippos/repositories"
	"github.com/h2non/gock"
)

func TestImportPropertiesFromUrl(t *testing.T) {
	defer gock.Off()

	reader := createReaderTest()
	repo := repositories.NewPropertieRepoMemory()

	gock.New("http://server.com").
		Get("properties.json").
		Reply(200).
		Body(reader)

	ImportPropertiesFromUrl(&repo, "http://server.com/properties.json")

	_, err := repo.GetById(1)

	if err != nil {
		t.Errorf("Expected propertie returned, but got %v", err.Error())
	}
}

func TestImportPropertiesFromUrlInvalidResponse(t *testing.T) {
	defer gock.Off()

	repo := repositories.NewPropertieRepoMemory()

	gock.New("http://server.com").
		Get("properties.json").
		ReplyError(errors.New("Request Error"))

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("Expected decode json error, but code did not panic")
		}
	}()

	ImportPropertiesFromUrl(&repo, "http://server.com/properties.json")
}

func TestImportPropertiesFromReader(t *testing.T) {
	reader := createReaderTest()
	repo := repositories.NewPropertieRepoMemory()

	ImportPropertiesFromReader(&repo, reader)

	_, err := repo.GetById(1)

	if err != nil {
		t.Errorf("Expected propertie returned, but got %v", err.Error())
	}
}

func TestImportPropertiesFromReaderInvalidJson(t *testing.T) {
	reader := strings.NewReader("foo")
	repo := repositories.NewPropertieRepoMemory()

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("Expected decode json error, but code did not panic")
		}
	}()

	ImportPropertiesFromReader(&repo, reader)
}

func createReaderTest() *strings.Reader {
	return strings.NewReader(`{
		"totalProperties": 8000,
		"properties": [
		  {
			"id": 1,
			"title": "Imóvel código 1, com 3 quartos e 2 banheiros.",
			"price": 643000,
			"description": "Laboris quis quis elit commodo eiusmod qui exercitation. In laborum fugiat quis minim occaecat id.",
			"lat": 1257,
			"long": 928,
			"beds": 3,
			"baths": 2,
			"squareMeters": 61
		  },
		  {
			"id": 2,
			"title": "Imóvel código 2, com 4 quartos e 3 banheiros.",
			"price": 949000,
			"description": "Anim mollit aliqua adipisicing labore magna pariatur aute nulla. Amet veniam ut voluptate aliquip esse officia adipisicing ipsum.",
			"lat": 679,
			"long": 680,
			"beds": 4,
			"baths": 3,
			"squareMeters": 94
		  },
		  {
			"id": 3,
			"title": "Imóvel código 3, com 5 quartos e 4 banheiros.",
			"price": 1779000,
			"description": "Et labore amet deserunt Lorem in tempor laboris esse in exercitation laboris nisi reprehenderit. Lorem dolor non cillum laboris voluptate aliquip.",
			"lat": 1051,
			"long": 441,
			"beds": 5,
			"baths": 4,
			"squareMeters": 174
		  }
		]
	}`)
}

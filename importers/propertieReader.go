package importers

import (
	"encoding/json"
	"io"
	"net/http"

	"bitbucket.org/caiotava/spotippos/entities"
	"bitbucket.org/caiotava/spotippos/repositories"
)

type propertieImportedCollection struct {
	TotalProperties int                 `json:"totalProperties"`
	Properties      []propertieImported `json:"properties"`
}

type propertieImported struct {
	Id           int      `json:"id"`
	Title        string   `json:"title"`
	Price        float32  `json:"price"`
	Description  string   `json:"description"`
	X            int      `json:"lat"`
	Y            int      `json:"long"`
	Beds         int      `json:"beds"`
	Baths        int      `json:"baths"`
	Provinces    []string `json:"provinces"`
	SquareMeters int      `json:"squareMeters"`
}

func ImportPropertiesFromUrl(repo repositories.PropertieRepository, u string) {
	reader, err := http.Get(u)

	if err != nil {
		panic(err)
	}

	ImportPropertiesFromReader(repo, reader.Body)
}

func ImportPropertiesFromReader(repo repositories.PropertieRepository, r io.Reader) {
	var c propertieImportedCollection

	decoder := json.NewDecoder(r)
	err := decoder.Decode(&c)

	if err != nil {
		panic(err)
	}

	for i := range c.Properties {
		p := c.Properties[i].createPropertie()

		repo.Save(&p)
	}
}

func (pi propertieImported) createPropertie() entities.Propertie {
	return entities.Propertie{
		Id:           pi.Id,
		Title:        pi.Title,
		Price:        pi.Price,
		Description:  pi.Description,
		X:            pi.X,
		Y:            pi.Y,
		Beds:         pi.Beds,
		Baths:        pi.Baths,
		SquareMeters: pi.SquareMeters,
	}
}
